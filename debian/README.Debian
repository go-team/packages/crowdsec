CrowdSec for Debian
===================

# Local API and Central API

There are multiple ways to configure `crowdsec`, leveraging a Local
API (LAPI) and/or the Central API (CAPI).


At the moment, the default configuration does the following:

 1. Set up a Local API locally, that doesn't listen on the
    network. This can be adjusted by following the
    [upstream local API documentation](https://doc.crowdsec.net/docs/local_api/intro).

 1. Register to the Central API by default, to take part in the
    collective effort. If that's not desired, it is possible to create
    a `/etc/crowdsec/online_api_credentials.yaml` file before
    installing the package, that contains a comment (e.g.
    `# no thanks`). In this case, the registration is skipped, and
    this file is also left behind in case the package is purged, so as
    to respective the admin's wishes if later reinstalled. If one
    reconsiders, it's sufficient to empty this file and run the
    following command manually:

        cscli capi register


# Hub management

## Offline hub

The `crowdsec` Debian package ships a copy of the available
collections (parsers, scenarios, and some other items) on the online
[hub](https://hub.crowdsec.net/) so that it can be configured out of
the box, without having to download anything from the internet. For
the purpose of this document, let's call this copy the “offline hub”.

Starting with the 1.4.6 release, only 3 collections (linux, apache2, and
nginx) are enabled automatically, as recommended by upstream. When
upgrading from earlier versions that enabled everything automatically,
*if* those 3 collections are still enabled, the postinst script makes
sure their dependencies are enabled as well. Otherwise, collections are
left untouched.

Enabling collections is achieved by creating symlinks below the
`/etc/crowdsec` directories, for collections, parsers, postoverflows,
and scenarios.


## Online hub

It is also possible to move away from the local, offline hub to the
online hub, so as to benefit from new or updated items without having
to wait for a package update. To do so, follow the upstream docs and
run:

    cscli hub update

Once that has happened, the offline hub will no longer be considered
and only items from the online hub will be used.

If going back to the offline hub is desired, that can be achieved by
running this command:

    /var/lib/dpkg/info/crowdsec.postinst disable-online-hub

It will undo the previous `enable-online-hub` action that happened
automatically when calling `cscli hub update` the first time,
but it might remove items that were available on the online hub, but
that are not on the offline hub. One might want to double check the
state of all configured collections afterward.

Once that has happened, don't forget to restart the crowdsec unit:

    systemctl restart crowdsec.service


## Implementation details

When configuring a collection, symlinks are created under
`/etc/crowdsec`, pointing at items under `/var/lib/crowdsec/hub`.

Initially, that directory points at items from the offline hub,
shipped under `/usr/share/crowdsec/hub`.

When switching to the online hub, the `/var/lib/crowdsec/hub`
directory no longer points at the offline hub, and contains a copy of
items downloaded from <https://hub.crowdsec.net/> instead.

If switching back to the offline hub, `/var/lib/crowdsec/hub` is
cleaned up (downloaded items are removed), and it starts pointing at
the offline hub again.


# Configuration management

The `crowdsec` package ships a few files under `/etc/crowdsec`, registered via
`conffiles`:

 - `/etc/crowdsec/acquis.yaml`
 - `/etc/crowdsec/config.yaml`
 - `/etc/crowdsec/patterns/*`
 - `/etc/crowdsec/profiles.yaml`
 - `/etc/crowdsec/simulation.yaml`

Starting with version 1.4.2, it's possible to add or override settings by using
a `.local` suffix for some of those files, as
[documented upstream](https://docs.crowdsec.net/docs/configuration/crowdsec_configuration/#configuration-files-that-support-yamllocal).

It's probably a good idea to use this mechanism so that further `crowsec`
updates don't generate too many `dpkg` prompts about configuration files that
have been modified locally.

Starting with version 1.4.2 as well, the `postinst` script performs some
detection regarding the database. If it's using the `sqlite` driver (the
default) and if it doesn't look like that file is stored over NFS, then a
`config.yaml.local` file is generated automatically, enabling the [WAL
optimization](https://www.sqlite.org/wal.html).

Upstream's default is an unconfigured
[`use_wal` setting](https://docs.crowdsec.net/docs/next/configuration/crowdsec_configuration/#use_wal)
which leads to warnings for everyone, so it was agreed with upstream to have the
Debian package try and guess the right value instead of leaving that up to each
and every `crowdsec` admin, and to store the result in the `config.yaml.local`
override.
