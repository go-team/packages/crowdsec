crowdsec (1.4.6-9) unstable; urgency=medium

  [ Nilesh Patra ]
  * Fix FTBFS with docker.io 26+ (Closes: #1077303):
     - Add 0020-add-patch-to-build-with-docker-26.patch
     - Version golang-github-docker-docker-dev (Build-)Depends accordingly.

 -- Cyril Brulebois <cyril@debamax.com>  Wed, 31 Jul 2024 12:03:56 +0200

crowdsec (1.4.6-8) unstable; urgency=medium

  * Add a symlink to config/crowdsec.service from debian/, and drop it
    from crowdsec.install, leaving it up to dh_installsystemd to choose
    where to install it (Closes: #1054084).

 -- Cyril Brulebois <cyril@debamax.com>  Sun, 14 Jul 2024 04:35:08 +0200

crowdsec (1.4.6-7) unstable; urgency=medium

  * Fix missing build-dependency on passwd, thanks to Santiago Vila for
    the bug report and the analysis (Closes: #1057549).

 -- Cyril Brulebois <cyril@debamax.com>  Thu, 14 Mar 2024 05:08:07 +0100

crowdsec (1.4.6-6) unstable; urgency=medium

  * Disable unreliable TestStreaming test, as seen under autopkgtest
    (https://github.com/crowdsecurity/crowdsec/issues/2352):
     - 0019-disable-unreliable-test-TestStreaming.patch

 -- Cyril Brulebois <cyril@debamax.com>  Fri, 14 Jul 2023 20:54:27 +0200

crowdsec (1.4.6-5) unstable; urgency=medium

  * Fix default acquis.yaml to also include the journalctl datasource,
    limited to the ssh.service unit, making sure acquisition works even
    without the traditional auth.log file (Closes: #1040976):
     - 0017-fix-default-acquisition.patch
  * Make sure an invalid datasource doesn't make the engine error out,
    making it possible to include the journalctl datasource in the default
    config file unconditionally, without having to worry whether
    journalctl is actually deployed and usable:
     - 0018-non-fatal-errors-for-invalid-datasources.patch

 -- Cyril Brulebois <cyril@debamax.com>  Thu, 13 Jul 2023 17:23:01 +0200

crowdsec (1.4.6-4) unstable; urgency=medium

  * Implement support for pending registration: since bouncers list crowdsec
    in Recommends, we cannot guarantee the order in which bouncers and
    crowdsec are configured (See: #1035499, #1036985). Bouncers can now
    queue triplets (systemd unit name, bouncer identifier and API key) in
    /var/lib/crowdsec/pending-registration. crowdsec.postinst will register
    those bouncers, and start their systemd units after removing that file
    (satisfying their ConditionPathExists=! on it).
  * Replace `exit 0` with `break` in the preceding code block.

 -- Cyril Brulebois <cyril@debamax.com>  Wed, 31 May 2023 18:54:17 +0200

crowdsec (1.4.6-3) unstable; urgency=medium

  * When performing an upgrade from pre-1.4.x versions, apply a workaround
    to avoid losing CAPI decisions for several hours (Closes: #1033138):
    delete alert(s) matching “Community blocklist”, and if at least one
    deletion occurred, restart the daemon to force an immediate pull.
  * Hardcode libsqlite3-0 (>= 3.35.0) in Depends to ensure Ent-generated
    SQLite queries are understood (Closes: #1033132): otherwise, we would
    get a dependency on libsqlite3-0 (>= 3.12.0) via shlibs, which is
    clearly not enough.
  * Backport upstream patch to fix building in the past/in the future (as
    seen with reproducible builds), no longer hardcoding the expected year
    for yearless timestamps:
     - 0016-try-to-make-reproducible-build-work-2119.patch

 -- Cyril Brulebois <cyril@debamax.com>  Sun, 19 Mar 2023 00:25:07 +0100

crowdsec (1.4.6-2) unstable; urgency=medium

  * Prefix package version with `v` when setting BUILD_VERSION, e.g.
    crowdsec/v1.4.6-2-linux-debian (Closes: #1031324).
  * Stop shipping a logrotate configuration snippet, as crowdsec rotates
    logs on its own via lumberjack.Logger, and that can be configured in
    the main configuration file (/etc/crowdsec/config.yaml):
     - Delete debian/crowdsec.logrotate
     - Add debian/crowdsec.conffiles, marking /etc/logrotate.d/crowdsec
       with the remove-on-upgrade flag.
  * When purging the package, remove internally-rotated log files, in
    addition to removing main log files and the logrotate-generated ones.
  * Really enable upstream-recommended collections, not just the items
    they require: everything would work fine without this, but those
    collections wouldn't appear in `cscli collections list`, and they
    wouldn't be upgraded if admins decided to switch to the online hub.
  * Compensate for the missing collections when upgrading from 1.4.6-1
    specifically (including binNMUs).
  * Enable the crowdsecurity/whitelists parser as well.
  * When performing a fresh install (as determined by the postinst's being
    called with just `configure`), and when /run/systemd/system exists,
    apply a workaround for upstream issue #2120 based on crowdsec.log:
     - Exit immediately if the unit is not active (e.g. the admin masked
       it before installing the package).
     - Check whether entries are received from the Central API, and exit
       if that's the case.
     - Check whether `received 0 new entries` is logged, and restart then
       exit if that's the case.
     - Repeat those checks every second, up to 20 times.
  * When performing an upgrade from pre-1.4.x versions, apply a workaround
    to avoid huge delays at the `restart` step: lower TimeoutStopSec from
    90s (default) to only 20s, using a runtime override (Closes: #1031326).
  * Add a dh_install-indep override to get rid of files left over after
    running the test suite (Closes: #1031328).

 -- Cyril Brulebois <cyril@debamax.com>  Fri, 17 Mar 2023 14:42:04 +0100

crowdsec (1.4.6-1) unstable; urgency=medium

  * New upstream release (Closes: #1031322).
  * Include a snapshot of hub files from the v1.4.6 branch, at commit
    f23a543a80.
  * Delete patch:
     - 0012-work-around-buggy-testparse-test.patch (fixed upstream)
  * Extend patch to avoid crowdsecurity/linux's being marked tainted:
     - 0004-disable-geoip-enrich.patch
  * Extend patch to skip more unreliable tests:
     - 0013-skip-flakky-tests.patch
  * Add patches:
     - 0014-silence-yaml-patching.patch: avoid polluting cscli's output
       with debug messages.
     - 0015-silence-not-latest-version.patch: upstream maintains a hub
       branch for our stable release (Closes: #1031323).
  * Rework collections handling:
     - With crowdsec growing over time, the initial “let's enable all
       collections” approach doesn't seem appropriate anymore.
     - On initial installation, only enable 3 collections (and their
       dependencies), which should cover common needs already:
        + crowdsecurity/linux
        + crowdsecurity/apache2
        + crowdsecurity/nginx
     - On upgrade, check whether all 3 collections are (still) enabled.
       If that's the case, enable their dependencies as well (as new
       versions tend to gain dependencies over time).
     - Let admins enable/disable any other collections on their own.
  * Update README.Debian accordingly.

 -- Cyril Brulebois <cyril@debamax.com>  Thu, 02 Mar 2023 05:07:10 +0000

crowdsec (1.4.2-3) unstable; urgency=medium

  * Really fix FTBFS with -A, by really adjusting the override.
  * Add patch:
     - 0013-skip-flakky-tests.patch: this should avoid build failures, and
       probably autopkgtest failures on “slow systems” (arm*, mips*).

 -- Cyril Brulebois <cyril@debamax.com>  Wed, 15 Feb 2023 10:03:10 +0000

crowdsec (1.4.2-2) unstable; urgency=medium

  * Fix FTBFS with -A (`Architecture: all` only) by adjusting the contents
    of the crowdsec binary package within override_dh_install-arch instead
    of override_dh_install.

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 14 Feb 2023 23:54:17 +0000

crowdsec (1.4.2-1) unstable; urgency=medium

  * New upstream release (Closes: #1011665).
  * Add patches:
     - 0008-r3labs-diff-versions.patch: adjust import path for
       r3labs/diff.
     - 0009-disable-kafka-acquisition-module.patch: this would require
       introducing a number of new packages.
     - 0010-disable-some-tests.patch: cloudwatch, kinesis, and docker are
       fine for integration tests but would require new packages and
       setting up a test infrastructure.
     - 0011-refresh-protobuf-code.patch: this avoids protobuf version
       mismatch.
     - 0012-work-around-buggy-testparse-test.patch: bump expected year
       when parsing year-less timestamps.
  * Refresh patches:
     - 0003-adjust-systemd-unit.patch
     - 0004-disable-geoip-enrich.patch
     - 0005-adjust-config.patch
     - 0007-automatically-enable-online-hub.patch
  * Delete obsolete patches:
     - 0001-use-a-local-machineid-implementation.patch (a separate package
       is available now)
     - 0002-add-compatibility-for-older-sqlite-driver.patch
     - 0006-prefer-systemctl-restart.patch (bug fixed upstream)
     - 0008-hub-disable-broken-scenario.patch
     - 0009-Improve-http-bad-user-agent-use-regexp-197.patch
     - 0010-5ae69aa293-fix-stacktrace-when-mmdb-files-are-not-present.patch
     - 0011-4dbbd4b3c4-automatically-download-files-when-needed.patch
  * Update dependencies:
     - Add golang-entgo-ent-dev
     - Add golang-github-alexliesenfeld-health-dev
     - Add golang-github-aquasecurity-table-dev
     - Add golang-github-beevik-etree-dev
     - Add golang-github-blackfireio-osinfo-dev
     - Add golang-github-c-robinson-iplib-dev
     - Add golang-github-confluentinc-bincover-dev
     - Add golang-github-crowdsecurity-dlog-dev
     - Add golang-github-crowdsecurity-grokky-dev
     - Add golang-github-crowdsecurity-machineid-dev
     - Add golang-github-hashicorp-go-plugin-dev
     - Add golang-github-ivanpirog-coloredcobra-dev
     - Add golang-github-jackc-pgx-v4-dev
     - Add golang-github-jarcoal-httpmock-dev
     - Add golang-github-jszwec-csvutil-dev
     - Add golang-github-masterminds-sprig-dev
     - Add golang-github-pbnjay-memory-dev
     - Add golang-github-r3labs-diff-dev
     - Add golang-github-slack-go-slack-dev
     - Add golang-github-texttheater-golang-levenshtein-dev
     - Add golang-github-xhit-go-simple-mail-dev
     - Bump golang-github-gin-gonic-gin-dev from 1.6.3 to 1.8.1
     - Delete golang-github-facebook-ent-dev (replaced with
       golang-entgo-ent-dev)
     - Delete golang-github-logrusorgru-grokky-dev (replaced with
       golang-github-crowdsecurity-grokky-dev)
     - Delete golang-github-olekukonko-tablewriter-dev (no longer used)
     - Replace golang-logrus-dev with golang-github-sirupsen-logrus-dev
     - Replace golang-pq-dev with golang-github-lib-pq-dev
     - Replace golang-prometheus-client-dev with
       golang-github-prometheus-client-golang-dev
     - Replace golang-yaml.v2-dev with golang-gopkg-yaml.v2-dev
     - Add python3 (for the testsuite).
  * Adjust variables passed to the build system via LD_FLAGS:
     - Set BUILD_CODENAME from platform/unix_common.mk
     - Set BUILD_TIMESTAMP, replacing BUILD_DATE.
     - Stop setting BUILD_GOVERSION, now determined at runtime.
  * Avoid “date: invalid date ‘@’” warnings when using ancillary rules.
  * Include a snapshot of hub files from the v1.4.2 branch, at commit
    73d2edaaf1.
  * Add README.source, documenting additional tarballs.
  * Include a snapshot of data files, at commit 03c7a30d70, plus
    Cloudflare IPv4 and IPv6 addresses as of 2022-12-15.
  * Upon upgrade, only enable items that are new since 1.0.9, instead
    of enabling all of them (which could enable again things that were
    disabled by the admin).
  * Adjust lintian overrides:
     - Adjust syntax to avoid mismatched overrides.
     - Extend hardening-no-pie to plugins.
     - Avoid a false positive with shell-script-fails-syntax-check.
     - Avoid a false positive with broken-gz.
     - Silence script-not-executable and unusual-interpreter bats
       for *.bats file (test files).

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 14 Feb 2023 22:32:27 +0000

crowdsec (1.0.9-4) experimental; urgency=medium

  * Add square brackets around paths for hardening-no-pie lintian overrides.
  * Add library package, which is a build dependency bouncers require.
  * Adjust debhelper control files for the switch from building a single
    package to building multiple packages (mostly marking existing files
    with a crowdsec. prefix).
  * Adjust setting BUILD_CODENAME, avoiding an awk(ward) warning.

 -- Cyril Brulebois <cyril@debamax.com>  Fri, 06 May 2022 04:48:13 +0000

crowdsec (1.0.9-3) unstable; urgency=medium

  * Backport upstream patches to deal with missing MMDB files gracefully
    (geolocation files aren't shipped by default):
      - 5ae69aa293: fix stacktrace when mmdb files are not present (#935)
      - 4dbbd4b3c4: automatically download files when needed (#895), so
        that switching to the online hub doesn't require extra steps to
        fetch files.

 -- Cyril Brulebois <cyril@debamax.com>  Sat, 04 Dec 2021 05:03:33 +0100

crowdsec (1.0.9-2) unstable; urgency=medium

  * Backport hub patch from upstream to fix false positives due to
    substring matches (https://github.com/crowdsecurity/hub/pull/197):
     + 0009-Improve-http-bad-user-agent-use-regexp-197.patch

 -- Cyril Brulebois <cyril@debamax.com>  Mon, 03 May 2021 07:29:06 +0000

crowdsec (1.0.9-1) unstable; urgency=medium

  * New upstream stable release:
     + Improve documentation.
     + Fix disabled Central API use case: without Central API credentials
       in the relevant config file, crowdsec would still try and establish
       a connection.
  * Add patch to disable broken scenario (ban-report-ssh_bf_report, #181):
     + 0008-hub-disable-broken-scenario.patch
  * Add logrotate config for /var/log/crowdsec{,_api}.log (weekly, 4).

 -- Cyril Brulebois <cyril@debamax.com>  Mon, 15 Mar 2021 01:19:43 +0100

crowdsec (1.0.8-2) unstable; urgency=medium

  * Update postinst to also strip ltsich/ when installing symlinks
    initially (new vendor in recent hub files, in addition to the usual
    crowdsecurity/).

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 02 Mar 2021 01:29:29 +0000

crowdsec (1.0.8-1) unstable; urgency=medium

  * New upstream stable release.
  * Refresh patches:
     + 0001-use-a-local-machineid-implementation.patch (unfuzzy)
     + 0002-add-compatibility-for-older-sqlite-driver.patch
  * Set cwversion variables through debian/rules (build metadata).
  * Add patch so that upstream's crowdsec.service is correct on Debian:
     + 0003-adjust-systemd-unit.patch
  * Really add lintian overrides for hardening-no-pie warnings.
  * Ship patterns below /etc/crowdsec/patterns: they're supposed to be
    stable over time, and it's advised not to modify them, but let's allow
    for some configurability.
  * Include a snapshot of hub files from the master branch, at commit
    d8a8509bdf: hub1. Further updates for a given crowdsec upstream
    version will be numbered hubN. After a while, they will be generated
    from a dedicated vX.Y.Z branch instead of from master.
  * Implement a generate_hub_tarball target in debian/rules to automate
    generating a tarball for hub files.
  * Add patch to disable geoip-enrich in the hub files as it requires
    downloading some files from the network that aren't under the usual
    MIT license:
     + 0004-disable-geoip-enrich.patch
  * Ship a selection of hub files in /usr/share/crowdsec/hub so that
    crowdsec can be set up without having to download data from the
    collaborative hub (https://hub.crowdsec.net/).
  * Ditto for some data files (in /usr/share/crowdsec/data).
  * Use DH_GOLANG_EXCLUDES to avoid including extra Go files from the
    hub into the build directory.
  * Implement an extract_hub_tarball target in debian/rules to automate
    extracting hub files from the tarball.
  * Implement an extract_data_tarball target in debian/rules to automate
    extracting data files from the tarball.
  * Ship crowdsec-cli (automated Golang naming) as cscli (upstream's
    preference).
  * Add patch to adjust the default config:
     + 0005-adjust-config.patch
  * Ship config/config.yaml accordingly, along with the config files it
    references.
  * Also adjust the hub_branch variable in config.yaml, pointing to the
    branch related to the current upstream release instead of master.
  * Create /var/lib/crowdsec/{data,hub} directories.
  * Implement configure in postinst to generate credentials files:
    Implement a simple agent setup with a Local API (LAPI), and with an
    automatic registration to the Central API (CAPI). The latter can be
    disabled by creating a /etc/crowdsec/online_api_credentials.yaml file
    containing a comment (e.g. “# no thanks”) before installing this
    package.
  * Implement purge in postrm. Drop all of /etc/crowdsec except
    online_api_credentials.yaml if this file doesn't seem to have been
    created during CAPI registration (likely because an admin created the
    file in advance to prevent it). Also remove everything below
    /var/lib/crowdsec/{data,hub}, along with log files.
  * Implement custom enable-online-hub and disable-online-hub actions in
    postinst. The latter is called once automatically to make sure the
    offline hub is ready to use. See README.Debian for details.
  * Also enable all items using the offline hub on fresh installation.
  * Add patch advertising `systemctl restart crowdsec` when updating the
    configuration: reload doesn't work at the moment (#656 upstream).
     + 0006-prefer-systemctl-restart.patch
  * Add patch automating switching from the offline hub to the online hub
    when `cscli hub update` is called:
     + 0007-automatically-enable-online-hub.patch
  * Add lintian override accordingly: uses-dpkg-database-directly.
  * Add ca-certificates to Depends for the CAPI registration.
  * Create /etc/machine-id if it doesn't exist already (e.g. in piuparts
    environments).

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 02 Mar 2021 00:25:48 +0000

crowdsec (1.0.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump copyright years.
  * Bump golang-github-facebook-ent-dev build-dep.
  * Swap Maintainer/Uploaders: the current plan is for me to keep in touch
    with upstream to coordinate packaging work in Debian. Help from fellow
    members of the Debian Go Packaging Team is very welcome, though!
  * Fix typos in the long description, and merge upstream's review.
  * Refresh patch:
     + 0001-use-a-local-machineid-implementation.patch
  * Drop patch (merged upstream):
     + 1001-fix-docker-container-creation-for-metabase-563.patch

 -- Cyril Brulebois <cyril@debamax.com>  Wed, 03 Feb 2021 08:54:24 +0000

crowdsec (1.0.2-1) unstable; urgency=medium

  * Initial release (Closes: #972573): start by shipping binaries,
    while better integration is being worked on with upstream:
    documentation and assisted configuration are coming up.
  * Version some build-deps as earlier versions are known not to work.
  * Use a local machineid implementation instead of depending on an
    extra package:
     + 0001-use-a-local-machineid-implementation.patch
  * Use a syntax that's compatible with version 1.6.0 of the sqlite3
    driver:
     + 0002-add-compatibility-for-older-sqlite-driver.patch
  * Backport upstream fix for golang-github-docker-docker-dev version
    currently in unstable:
     + 1001-fix-docker-container-creation-for-metabase-563.patch
  * Install all files in the build directory so that the testsuite finds
    required test data that's scattered all over the place.
  * Add systemd to Build-Depends for the testsuite, so that it finds
    the journalctl binary.
  * Add lintian overrides for the hardening-no-pie warnings: PIE is not
    relevant for Go packages.

 -- Cyril Brulebois <cyril@debamax.com>  Thu, 14 Jan 2021 02:46:18 +0000
