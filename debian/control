Source: crowdsec
Maintainer: Cyril Brulebois <cyril@debamax.com>
Uploaders: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-entgo-ent-dev,
               golang-github-alecaivazis-survey-dev,
               golang-github-alexliesenfeld-health-dev,
               golang-github-antonmedv-expr-dev,
               golang-github-appleboy-gin-jwt-dev,
               golang-github-aquasecurity-table-dev,
               golang-github-beevik-etree-dev,
               golang-github-blackfireio-osinfo-dev,
               golang-github-buger-jsonparser-dev,
               golang-github-c-robinson-iplib-dev,
               golang-github-confluentinc-bincover-dev,
               golang-github-coreos-go-systemd-dev,
               golang-github-crowdsecurity-dlog-dev,
               golang-github-crowdsecurity-grokky-dev,
               golang-github-crowdsecurity-machineid-dev,
               golang-github-davecgh-go-spew-dev,
               golang-github-dghubble-sling-dev,
               golang-github-docker-docker-dev (>= 26),
               golang-github-docker-go-connections-dev,
               golang-github-enescakir-emoji-dev,
               golang-github-gin-gonic-gin-dev (>= 1.8.1),
               golang-github-go-co-op-gocron-dev,
               golang-github-go-openapi-errors-dev,
               golang-github-go-openapi-strfmt-dev,
               golang-github-go-openapi-swag-dev,
               golang-github-go-openapi-validate-dev,
               golang-github-go-sql-driver-mysql-dev,
               golang-github-google-go-querystring-dev,
               golang-github-goombaio-namegenerator-dev,
               golang-github-hashicorp-go-plugin-dev,
               golang-github-hashicorp-go-version-dev,
               golang-github-ivanpirog-coloredcobra-dev,
               golang-github-jackc-pgx-v4-dev,
               golang-github-jarcoal-httpmock-dev,
               golang-github-jszwec-csvutil-dev,
               golang-github-lib-pq-dev,
               golang-github-masterminds-sprig-dev,
               golang-github-mattn-go-sqlite3-dev,
               golang-github-mohae-deepcopy-dev,
               golang-github-nxadm-tail-dev,
               golang-github-opencontainers-image-spec-dev,
               golang-github-oschwald-geoip2-golang-dev (>= 1.2),
               golang-github-oschwald-maxminddb-golang-dev (>= 1.4),
               golang-github-pbnjay-memory-dev,
               golang-github-pkg-errors-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-prom2json-dev,
               golang-github-r3labs-diff-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-slack-go-slack-dev,
               golang-github-spf13-cobra-dev,
               golang-github-stretchr-testify-dev,
               golang-github-texttheater-golang-levenshtein-dev,
               golang-github-xhit-go-simple-mail-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-mod-dev,
               golang-golang-x-sys-dev,
               golang-gopkg-natefinch-lumberjack.v2-dev,
               golang-gopkg-tomb.v2-dev,
               golang-gopkg-yaml.v2-dev,
               passwd,
               python3,
               systemd
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/crowdsec
Vcs-Git: https://salsa.debian.org/go-team/packages/crowdsec.git
Homepage: https://github.com/crowdsecurity/crowdsec
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/crowdsecurity/crowdsec

Package: crowdsec
Architecture: any
Depends: ca-certificates,
         ${misc:Depends},
         ${shlibs:Depends},
         libsqlite3-0 (>= 3.35.0),
Built-Using: ${misc:Built-Using}
Description: lightweight and collaborative security engine
 CrowdSec is a lightweight security engine, able to detect and remedy
 aggressive network behavior. It can leverage and also enrich a
 global community-wide IP reputation database, to help fight online
 cybersec aggressions in a collaborative manner.
 .
 CrowdSec can read many log sources, parse and also enrich them, in
 order to detect specific scenarios, that usually represent malevolent
 behavior. Parsers, Enrichers, and Scenarios are YAML files that can
 be shared and downloaded through a specific Hub, as well as be created
 or adapted locally.
 .
 Detection results are available for CrowdSec, its CLI tools and
 bouncers via an HTTP API. Triggered scenarios lead to an alert, which
 often results in a decision (e.g. IP banned for 4 hours) that can be
 consumed by bouncers (software components enforcing a decision, such
 as an iptables ban, an nginx lua script, or any custom user script).
 .
 The CLI allows users to deploy a Metabase Docker image to provide
 simple-to-deploy dashboards of ongoing activity. The CrowdSec daemon
 is also instrumented with Prometheus to provide observability.
 .
 CrowdSec can be used against live logs (“à la fail2ban”), but can
 also work on cold logs to help, in a forensic context, to build an
 analysis for past events.
 .
 On top of that, CrowdSec aims at sharing detection signals amongst
 all participants, to pre-emptively allow users to block likely
 attackers. To achieve this, minimal meta-information about the attack
 is shared with the CrowdSec organization for further retribution.
 .
 Users can also decide not to take part into the collective effort via
 the central API, but to register on a local API instead.

Package: golang-github-crowdsecurity-crowdsec-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-entgo-ent-dev,
         golang-github-alecaivazis-survey-dev,
         golang-github-alexliesenfeld-health-dev,
         golang-github-antonmedv-expr-dev,
         golang-github-appleboy-gin-jwt-dev,
         golang-github-aquasecurity-table-dev,
         golang-github-beevik-etree-dev,
         golang-github-blackfireio-osinfo-dev,
         golang-github-buger-jsonparser-dev,
         golang-github-c-robinson-iplib-dev,
         golang-github-confluentinc-bincover-dev,
         golang-github-coreos-go-systemd-dev,
         golang-github-crowdsecurity-dlog-dev,
         golang-github-crowdsecurity-grokky-dev,
         golang-github-crowdsecurity-machineid-dev,
         golang-github-davecgh-go-spew-dev,
         golang-github-dghubble-sling-dev,
         golang-github-docker-docker-dev (>= 26),
         golang-github-docker-go-connections-dev,
         golang-github-enescakir-emoji-dev,
         golang-github-gin-gonic-gin-dev (>= 1.8.1),
         golang-github-go-co-op-gocron-dev,
         golang-github-go-openapi-errors-dev,
         golang-github-go-openapi-strfmt-dev,
         golang-github-go-openapi-swag-dev,
         golang-github-go-openapi-validate-dev,
         golang-github-go-sql-driver-mysql-dev,
         golang-github-google-go-querystring-dev,
         golang-github-goombaio-namegenerator-dev,
         golang-github-hashicorp-go-plugin-dev,
         golang-github-hashicorp-go-version-dev,
         golang-github-ivanpirog-coloredcobra-dev,
         golang-github-jackc-pgx-v4-dev,
         golang-github-jarcoal-httpmock-dev,
         golang-github-jszwec-csvutil-dev,
         golang-github-lib-pq-dev,
         golang-github-masterminds-sprig-dev,
         golang-github-mattn-go-sqlite3-dev,
         golang-github-mohae-deepcopy-dev,
         golang-github-nxadm-tail-dev,
         golang-github-opencontainers-image-spec-dev,
         golang-github-oschwald-geoip2-golang-dev (>= 1.2),
         golang-github-oschwald-maxminddb-golang-dev (>= 1.4),
         golang-github-pbnjay-memory-dev,
         golang-github-pkg-errors-dev,
         golang-github-prometheus-client-golang-dev,
         golang-github-prometheus-client-model-dev,
         golang-github-prometheus-prom2json-dev,
         golang-github-r3labs-diff-dev,
         golang-github-sirupsen-logrus-dev,
         golang-github-slack-go-slack-dev,
         golang-github-spf13-cobra-dev,
         golang-github-stretchr-testify-dev,
         golang-github-texttheater-golang-levenshtein-dev,
         golang-github-xhit-go-simple-mail-dev,
         golang-golang-x-crypto-dev,
         golang-golang-x-mod-dev,
         golang-golang-x-sys-dev,
         golang-gopkg-natefinch-lumberjack.v2-dev,
         golang-gopkg-tomb.v2-dev,
         golang-gopkg-yaml.v2-dev,
         python3,
         ${misc:Depends}
Description: lightweight and collaborative security engine - library
 CrowdSec is a lightweight security engine, able to detect and remedy
 aggressive network behavior. It can leverage and also enrich a
 global community-wide IP reputation database, to help fight online
 cybersec aggressions in a collaborative manner.
 .
 This package contains the development files.
