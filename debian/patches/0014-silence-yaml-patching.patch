From: Cyril Brulebois <cyril@debamax.com>
Date: Wed, 01 Mar 2023 22:36:00 +0100
Subject: Silence yaml patching

Being able to patch configuration files using .local snippets is relatively new,
and very useful when it comes to shipping mostly untouched configuration files,
with just a few values set in a .local file.

Unfortunately the initial code is very chatty and generates info-level messages
in all cscli calls.

An early patch upstream is ca12432a2acd2fd607e9fbea97fea3fb3f124678 (backported
here), which demotes those messages from info to debug. It left the “Prepending”
messages at info, but those haven't been seen with the current packages, so
that's left untouched in this patch as well.

Further refinement landed in cd4dabde0ec833552881dd36780ab847cf20882d but that
touches more code, and we're closing in freeze-wise, so leave it alone. All our
crowdsec* packages ship a README.Debian file, which should be sufficient in most
cases. When in doubt, adjusting the log level in specific commands should be
enough to figure out what's happening.

--- a/pkg/yamlpatch/patcher.go
+++ b/pkg/yamlpatch/patcher.go
@@ -60,7 +60,7 @@ func (p *Patcher) MergedPatchContent() (
 		return nil, err
 	}
 	if err == nil {
-		log.Infof("Patching yaml: '%s' with '%s'", p.BaseFilePath, p.PatchFilePath)
+		log.Debugf("Patching yaml: '%s' with '%s'", p.BaseFilePath, p.PatchFilePath)
 	}
 
 	var patched *bytes.Buffer
